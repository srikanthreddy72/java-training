package ABCollege;

public class Students {

    public String studentName;
    public String studentRollNo;


    @Override
    public boolean equals(Object obj){
        if(obj == this) return true;
        if(obj == null || getClass() != obj.getClass()) return false;

        return (this.getStudentName() == ((Students) obj).getStudentName() && this.getStudentRollNo() == ((Students) obj).getStudentRollNo());
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentRollNo() {
        return studentRollNo;
    }

    public void setStudentRollNo(String studentRollNo) {
        this.studentRollNo = studentRollNo;
    }
}


package ABCollege;

import java.util.Set;

public class StdClass {

    private String className;
    private int TotalStudents;
    public Set<Students> studentSet;

    @Override
    public boolean equals(Object obj){
        if(obj == this) return true;
        if(obj == null || getClass() != obj.getClass()) return false;

        StdClass cs= (StdClass)obj;
        return (this.getClassName().equals(cs.getClassName()) && this.getTotalStudents() == cs.getTotalStudents() && this.getStudentSet().equals(cs.getStudentSet()));
    }

    public Set<Students> getStudentSet() {
        return studentSet;
    }

    public void setStudentSet(Set<Students> studentSet) {
        this.studentSet = studentSet;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getTotalStudents() {
        return TotalStudents;
    }

    public void setTotalStudents(int totalStudents) {
        TotalStudents = totalStudents;
    }
}

package ABCollege;

import java.util.List;

public class College {

    private String collegeName;
    private String collegeAddress;

    public List<StdClass> stdClassList;


    @Override
    public boolean equals(Object obj){
        if(obj == this) return true;
        if(obj == null || getClass() != obj.getClass()) return false;

        College cc= (College)obj;
        return (this.getCollegeName().equals(cc.getCollegeName()) && this.getCollegeAddress().equals(cc.getCollegeAddress()) && this.getClassList().equals(cc.getClassList()));
    }

    public List<StdClass> getClassList() {
        return stdClassList;
    }

    public void setClassList(List<StdClass> stdClassList) {
        this.stdClassList = stdClassList;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getCollegeAddress() {
        return collegeAddress;
    }

    public void setCollegeAddress(String collegeAddress) {
        this.collegeAddress = collegeAddress;
    }
}

package ABCollege;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StudentImpl {

    public static void main(String []args){


        Students s1 = new Students();
        Students s2 = new Students();

        s1.setStudentName("Rakesh");
        s1.setStudentRollNo("R1234");

        s2.setStudentName("Ramesh");
        s2.setStudentRollNo("R235");

        StdClass cs1 = new StdClass();
        StdClass cs2 = new StdClass();

        cs1.setClassName("Degree");
        cs1.setTotalStudents(134);

        cs2.setClassName("Inter");
        cs2.setTotalStudents(137);

        Set<Students> studentsList = new HashSet<Students>();
        studentsList.add(s1);
        studentsList.add(s2);

        College cc = new College();
        cc.setCollegeName("CDC");
        cc.setCollegeAddress("HNK");

        List<StdClass> stdClassList = new ArrayList<StdClass>();
        stdClassList.add(cs1);
        stdClassList.add(cs2);


        System.out.println(cs1.equals(cs1));
        System.out.println(cs1.equals(cs2));
        System.out.println(s1.equals(s2));


    }
}
