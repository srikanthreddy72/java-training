package DuckSimulator;

public interface FlyBehavior {
    public void fly();
}

package DuckSimulator;

public interface QuackBehavior {
    public void quack();
}


package DuckSimulator;

public abstract class Duck {

    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior;

    public Duck() {

    }

    public abstract void display();

    public void performFly() {
        this.flyBehavior.fly();
    }

    public void performQuack() {
        this.quackBehavior.quack();
    }

    public void setFlyBehavior(FlyBehavior fb) {
        this.flyBehavior = fb;
    }

    public void setQuackBehavior(QuackBehavior qb) {
        this.quackBehavior = qb;
    }

}

package DuckSimulator;

public class BrownDuck extends  Duck{

    public  BrownDuck() {

        this.flyBehavior = new FlyWithWings();

        this.quackBehavior = new Quack();
    }

    @Override
    public void display() {
        System.out.println("I'm a Brown duck!");
    }
}


package DuckSimulator;

public class DecoyDuck extends Duck{

    public DecoyDuck(){

        this.flyBehavior = new FlyNoWay();
        this.quackBehavior = new QuackNoWay();
    }

    @Override
    public void display() {
        System.out.println("I'm a Decoy duck!");
    }
}

package DuckSimulator;

public class MallardDuck extends Duck {

    public MallardDuck() {


        this.quackBehavior = new Quack();

        this.flyBehavior = new FlyWithWings();
    }

    @Override
    public void display() {
        System.out.println("I'm a mallard duck!");
    }
}

package DuckSimulator;

public class RubberDuck extends Duck{

    public RubberDuck() {

        this.quackBehavior = new Quack();

        this.flyBehavior = new FlyNoWay();
    }

    @Override
    public void display() {
        System.out.println("I'm a Rubber duck!");
    }
}


package DuckSimulator;

public class WhiteDuck extends  Duck{

    public WhiteDuck() {

        this.flyBehavior = new FlyWithWings();

        this.quackBehavior = new Quack();
    }

    @Override
    public void display() {
        System.out.println("I'm a white duck!");
    }
}


package DuckSimulator;

public class FlyNoWay implements FlyBehavior {

    @Override
    public void fly() {
        System.out.println("I can't fly!");
    }
}

package DuckSimulator;

public class FlyWithWings implements FlyBehavior {

    @Override
    public void fly() {
        System.out.println("I'm flying!");
    }
}

package DuckSimulator;


public class Quack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("I can Quack!");
    }
}

package DuckSimulator;

public class QuackNoWay implements QuackBehavior {

    @Override
    public void quack() {
        System.out.println("I can't Quack!");
    }
}


package DuckSimulator;

public class DuckSimulatorImpl {

    private static void newRubberDuck(){
        Duck Rubber=new RubberDuck();

        Rubber.performFly();
        Rubber.performQuack();
        Rubber.display();
    }

    private static void newDecoyDuck(){
        Duck Decoy=new DecoyDuck();

        Decoy.performFly();
        Decoy.performQuack();
        Decoy.display();
    }



    public static void main(String[] args){
        newRubberDuck();
        newDecoyDuck();
    }
}




